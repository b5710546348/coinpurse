package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Patchara Pattiyathanee 5710546348
 */
public class Coin extends AbstractValuable{

    /** Value of the coin */
    private double value;
    private String currency;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value , String currency) {
      this.value = value;
      this.currency = currency;
    }
    
    /**
     * to return coin value.
     * @return value of this coin.
     */
    public double getValue(){
    	return this.value;
    }
    
    /**
     * @return Coin value in Baht unit.
     */
    public String toString(){
    	return (int)this.value+ this.currency + " coin ";
    }
    
   

	
    
}

