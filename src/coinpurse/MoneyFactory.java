package coinpurse;

import java.util.ResourceBundle;
/**
 * This class are model for money currency class.
 * @author Patchra Pattiyathanee 5710546348
 * @version 28 April 2015
 */
public abstract class MoneyFactory {
	private static MoneyFactory mf;
	
	/**
	 * Constructor of this class , mf = null at first time.
	 */
	protected MoneyFactory(){
		mf = this;
	}
	
	/**
	 * To return this object after set new instance .
	 * @return this object
	 */
	static MoneyFactory getInstance(){
		setFactory();
		return mf;
	}
	
	//abstract class for create new coin or banknote if value are reasonable.
	abstract Valuable createMoney(double value) throws IllegalArgumentException;
		
	/**
	 * To return double value from string
	 * @param value
	 * @return value in double type
	 */
	public Valuable createMoney(String value){
		return mf.createMoney(Double.parseDouble(value));
	}
	
	/**
	 * To set new instance of whole class ,access by resource bundle.
	 */
	public static void setFactory(){
		ResourceBundle rb = ResourceBundle.getBundle("purse");
		String temp = rb.getString("type1");
		try {
			mf = (MoneyFactory) Class.forName(temp).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			mf = new MalaiMoneyFactory();
			//e.printStackTrace();
		}
		
	}

	
}
