package coinpurse;
 

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A coin purse contains money.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  money to remove.
 *  
 *  @author Patchara Pattiyathanee 5710546348
 */
public class Purse extends Observable{
    /** Collection of money in the purse. */
	private ArrayList<Valuable> purse ;

    /** Capacity is maximum NUMBER of money the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    /**
     * strategy of withdraw also can change. 
     */
    private WithdrawStrategy strategy ;
   
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of money you can put in purse.
     */
    public Purse( int capacity ) {
    	this.capacity = capacity;
    	purse = new ArrayList<Valuable>();
    }

    /**
     * Count and return the number of money in the purse.
     * This is the number of money, not their value.
     * @return the number of money in the purse
     */
    
    public int count() { 

    	return purse.size(); 
    }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    
    public double getBalance() {
    	double value = 0;
    	for(int i = 0 ; i < purse.size() ; i++){
    		value += purse.get(i).getValue();
    	}
    	return value;
    }

    
    /**
     * Return the capacity of the coin purse.
     * @return the capacity
     */
    
    public int getCapacity() { 

    	return this.capacity; 
    }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    
    public boolean isFull() {
        if(purse.size()==this.capacity){
        	
        	return true;
        }
        else{
        	return false;
        }
    }

    /** 
     * Insert a coin into the purse.
     * The coin is only inserted if the purse has space for it
     * and the coin has positive value.  No worthless money!
     * @param coin is a Coin object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    
    public boolean insert( Valuable valuable ) {
       if(isFull()){
    	  
    	   return false;
       }
       else{
    	   purse.add(valuable);
    	   super.setChanged();
    	   super.notifyObservers(this);
    	   return true;
    	  
       }
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of money withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    
    public Valuable[] withdraw( double amount ) {
        	List<Valuable> temp_purse = new ArrayList<Valuable>(); 
            
            temp_purse = strategy.withdraw(amount, purse);
            
            if(temp_purse==null ){
            	return null;
            }
            
            Valuable [] money1 = new Valuable[temp_purse.size()];
			for(int i = 0 ; i < temp_purse.size() ; i++){
				money1[i] = temp_purse.get(i);
			}
			super.setChanged();
	    	super.notifyObservers(this);
	  
		return money1;
	}
    /**
     * set new strategy
     * @param withdrawstrategy
     */
    public void setWithdrawStrategy(WithdrawStrategy withdrawstrategy){
    		this.strategy = withdrawstrategy;
    }
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
        String message = "";
    	for(int i = 0 ; i < purse.size() ; i++){
        	message += purse.get(i).toString()+" ";
        }
    	return "Purse with "+message;
    }

}
