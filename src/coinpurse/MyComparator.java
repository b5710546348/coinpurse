package coinpurse;

import java.util.Comparator;
/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 *
 */
public class MyComparator implements Comparator<Valuable>{
	public int compare(Valuable a , Valuable b){
		
			return (int) Math.signum((a.getValue() - b.getValue()));
		
		
	}
}
