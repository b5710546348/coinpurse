package coinpurse;
/**
 * Abstract class that can work instead of Valuable interface.
 * @author Patchara Pattiyathanee 5710546348
 *
 */
public abstract class AbstractValuable implements Valuable {
	
	
	/**
	 * compare 2 objects value. 
	 * @param valuable
	 * @return several value depend on value of this object and value of other object.
	 */
	public int compareTo(Valuable valuable){
		if(this.getValue() > valuable.getValue()){
			return 1;
		}
		if(this.getValue() < valuable.getValue()){
			return -1;
		}
	
		return 0;
	
	}
	/**
	 * check equality of object.
	 * @param receive unknown type Object obj.
	 * @return ture if object have same value.
	 */
	public boolean equals(Object obj){
		if(obj!=null){
			if(obj.getClass()==this.getClass()){
				if(this.getValue()==((Valuable)obj).getValue()){
					return true;
				}
			}
		}
		return false;
	}
	
	
}
