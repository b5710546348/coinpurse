package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 24.2.2015
 */
public class PurseStatusObserver extends JFrame implements Observer{
	
	//windows of gui
	private JFrame frame;
	//status in purse
	private JLabel status = new JLabel("");
	//pane of program
	private JPanel pane;
	//String of status in purse
	private String current_status;
	//bar of status in purse
	private JProgressBar bar = new JProgressBar();
	
	/**
	 * constructor if Purse Status Observer
	 */
	public PurseStatusObserver(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setSize(300, 100);
		frame.setLocation(100, 300);
		frame.setResizable(false);
		
	}

	/**
	 * update status in purse.
	 */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			if(purse.isFull()){
				status.setText("FULL");
			}
			if(purse.count()==0){
				status.setText("EMPTY");
			}
			else{
				current_status = purse.count()+"";
				status.setText(current_status);
				bar.setValue(purse.count());
			}
		}
		if(info != null){
			System.out.print(info);
		}
		
	}
	
	/**
	 * all of component of program.
	 */
	public void initcomponent(){
		frame.setTitle("Purse Balance");
	    pane = new JPanel();
	    pane.setLayout(new BoxLayout(pane ,BoxLayout.Y_AXIS));
		pane.add(status);
		pane.add(bar);
		bar.setMaximum(10);
		frame.add(pane);
		frame.setVisible(true);
	}
	
	/**
	 * run the program
	 */
	public void run(){
		this.initcomponent();
		
	}

}
