package coinpurse;

/**
 * every class that implement this interface must have method in below.
 * @author Patchara Pattiyathanee 5710546348
 *
 */
public interface Valuable extends Comparable<Valuable>{
	public double getValue();

	
}
