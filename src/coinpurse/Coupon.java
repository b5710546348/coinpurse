package coinpurse;
import java.util.*;

/** A coupon with color,each color of coupon have different color.
	 * @author Patchara Pattiyathanee 5710546348
	 */
public class Coupon extends AbstractValuable{
	
	
	/** color type */
	private String color;
	
	/** declare map type to collect a couple of data.*/
	private Map <String,Double> map;
	
	/**
	 * constructor of this coupon
	 * @param initial color and declare each value in coupon color by use map type.
	 */
	public Coupon(String color){
		//super(map);
		this.color = color.toLowerCase();
		map = new HashMap<String,Double>();
		map.put("red", 100.0);
		map.put("green", 20.0);
		map.put("blue", 50.0);
		
	}

	/**
	 *to return coupon value
	 * @return value of coupon color.
	 */
	public double getValue() {
		return map.get(color);
	}
	
	
	/**
	 * message returning
	 * @return color of coupon in String.
	 */
	public String toString(){
		return this.color+" coupon";
	}

	
}
