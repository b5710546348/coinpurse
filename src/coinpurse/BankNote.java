package coinpurse;

/**
 * A banknoe with value and serial number 
 * @author Patchara Pattiyathanee 5710546348
 *
 */
public class BankNote extends AbstractValuable {
	/** value of banknote**/
	private double value;
	/** serial number by initial at 1000000 */
	private static double serialnumber  = 1000000;
	private String currency;
	
	
	/**
	 * Constructor of BankNote.Auto generated serial number. 
	 * @param value of each banknote
	 */
	public BankNote(double value , String currency){
		this.value = value;
		this.serialnumber = this.getNextSerialNumber();
		this.currency = currency;
	}
	
	
	/**
	 * increased serial number
	 * @return current serial number + 1 ,this will auto generated serial number.
	 */
	public static double getNextSerialNumber(){
		return serialnumber++;
	}
	
	/**
     * to return coin value.
     * @return value of this coin.
     */
    public double getValue(){
    	return this.value;
    }
	/**
	 * message returning
	 * @return String with value and serial number
	 */
	public String toString(){
		return String.format("%.0f-%s Banknote [%.0f]", this.getValue() ,this.currency ,this.serialnumber);
	}

	
	
	

}
