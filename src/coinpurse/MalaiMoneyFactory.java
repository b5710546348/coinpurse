package coinpurse;
/**
 * This class use to declare new coin in Malaysia currency if value are reasonable to real coin.
 * @author Patchara Pattiyathanee 5710546348
 * @version 28 April 2015
 */
public class MalaiMoneyFactory extends MoneyFactory{


	/**
	 * use for declare new coin and banknote if value are correct to real word money.
	 * @param value of unknown source money
	 */
	Valuable createMoney(double value)  {
		if(value == 0.05 || value == 0.1 || value == 0.2 || value == 0.5 ){
			Coin coin = new Coin(value*100,"Sen");
			return coin;
		}
		else if (value == 1.0 || value == 2.0 || value == 5.0 || value == 10.0 
				|| value == 20.0 || value == 50.0 || value == 100.0){
			BankNote banknote = new BankNote(value , "Ringgit");
			return banknote;
		}
		
		else
			throw new IllegalArgumentException();
		
	}

}
