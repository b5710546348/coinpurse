package coinpurse;
/**
 * This class use to declare new coin in Thai currency if value are reasonable to real coin.
 * @author Patchara Pattiyathanee 5710546348
 * @version 28 April 2015
 */
public class ThaiMoneyFactory extends MoneyFactory{
	
	
	/**
	 * use for declare new coin and banknote if value are correct to real word money.
	 * @param value of unknown source money
	 */
	Valuable createMoney(double value) {
		if(value == 1.0 || value == 2.0 || value == 5.0 || value == 10.0 ){
			Coin coin = new Coin(value , "Baht");
			return coin;
		}
		else if(value == 20.0 || value == 50.0 || value == 100.0 || value == 500.0 || value == 1000.0){
			BankNote banknote = new BankNote(value , "Baht");
			return banknote;
		}
		else
			throw new IllegalArgumentException();
		
	}
}
