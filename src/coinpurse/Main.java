package coinpurse;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
    	Purse  purse1 = new Purse(10);
    	WithdrawStrategy strategy = new GreedyWithdraw();
        purse1.setWithdrawStrategy(strategy);
    	PurseBalanceObserver p = new PurseBalanceObserver();
        purse1.addObserver(p);
        p.run();
        PurseStatusObserver  s = new PurseStatusObserver();
        purse1.addObserver(s);
        s.run();
        ConsoleDialog dialog1 = new ConsoleDialog(purse1);
       
    	dialog1.run();
    }
}
