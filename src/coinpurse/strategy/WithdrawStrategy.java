package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
/**
 * 
 * @author Patchara Patiyathanee 5710546348
 *
 */
public interface WithdrawStrategy{
	public List<Valuable> withdraw(double amount , List<Valuable> valuables);
}
