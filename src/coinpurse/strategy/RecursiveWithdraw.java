package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.MyComparator;
import coinpurse.Valuable;
import coinpurse.Purse;
/**
 * This strategy use recursion to withdraw item in purse.
 * @author Patchara Pattiyathanee 5710546348
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy{

	/* @param amount to withdraw and list of purse.
	 * @return 
	 */
	 
	public List<Valuable> withdraw(double amount, List<Valuable> list) {
		List<Valuable> returnTemp = withdrawSupport(amount,list,0);
		if(returnTemp==null){
			if(list.size()==1){
				return null;
			}
			else{
				return withdraw(amount,list.subList(1, list.size()));
			}
			
		}
		
		return returnTemp;
		
	}
	
	public List<Valuable> withdrawSupport(double amount,List<Valuable> list,int index){

		if(list.size()==0 ||index == list.size() || amount<0){
			return null;
		}
		
		amount -= list.get(index).getValue();
		
		if(amount==0){
			List <Valuable> tempList = new ArrayList();
			tempList.add(list.get(index));
			return tempList;
		}

		List <Valuable> returnList = withdrawSupport(amount,list,index+1);

		if(list==null){
			amount+=list.get(index).getValue();
			return withdrawSupport(amount,list,index+1);
		}
		else{
			returnList.add(list.get(index));
			return returnList;
		}
	}
	

}
