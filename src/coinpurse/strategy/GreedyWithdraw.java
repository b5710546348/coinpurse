package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import coinpurse.MyComparator;
import coinpurse.Valuable;

/**
 * This class use greedy algorithm to withdraw money in purse.
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 24.2.2015
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {
	/**
	 * this method will withdraw money by amount if it fit with money in purse.
	 * 
	 * @param amount
	 *            to withdraw and list of money
	 * @return list of money
	 */
	public List<Valuable> withdraw(double amount, List<Valuable> list) {

		List<Valuable> tempList = new<Valuable> ArrayList();
		if (amount == 0)
			return null;

		for (int i = 0; i < list.size(); i++) {
			if (amount == 0) {
				break;
			}
			if (amount < 0) {
				return null;
			}
			if (list.get(i).getValue() > amount){
				continue;
			}
			
			tempList.add(list.get(i));
			amount -= list.get(i).getValue();

		}

		if (amount != 0)
			return null;

		return tempList;
	}

}
