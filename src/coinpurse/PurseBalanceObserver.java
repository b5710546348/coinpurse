package coinpurse;
import javax.swing.*;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;
/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 24.2.2015
 */
public class PurseBalanceObserver extends JFrame implements Observer{
	
	//windows of gui
	private JFrame frame;
	//balance in purse
	private JLabel balance = new JLabel("0.00 Baht");
	//pane of program
	private JPanel pane;
	//String of balance in purse
	private String current_balance;
	
	/**
	 * constructor if Purse Balance Observer
	 */
	public PurseBalanceObserver(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setSize(300, 100);
		frame.setLocation(100, 100);
		frame.setResizable(false);
		
	}

	/**
	 *  update balance in purse.
	 */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			current_balance = purse.getBalance()+" Baht";
			balance.setText(current_balance);
		}
		if(info != null){
			System.out.print(info);
		}
		
	}
	
	/**
	 * all of component of program.
	 */
	public void initcomponent(){
		frame.setTitle("Purse Balance");
	   
		pane = new JPanel();
		pane.setLayout(new BoxLayout(pane ,BoxLayout.X_AXIS));
		
		pane.add(balance);
		frame.add(pane);
		 frame.setVisible(true);
	}
	
	/**
	 * run the program
	 */
	public void run(){
		this.initcomponent();
		
	}
}
